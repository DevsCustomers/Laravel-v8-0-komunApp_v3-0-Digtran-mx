<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

//resources
Route::resource('/users', App\Http\Controllers\UsersController::class);

Route::resource('/releases', App\Http\Controllers\ReleasesController::class);

Route::resource('/payments', App\Http\Controllers\PaymentsController::class);

Route::resource('/parkingmeters', App\Http\Controllers\ParkingmetersController::class);

Route::resource('/schedules', App\Http\Controllers\SchedulesController::class);

Route::resource('/profile', App\Http\Controllers\ProfilesController::class);

Route::resource('/messages', App\Http\Controllers\MessagesController::class);

Route::resource('/activities', App\Http\Controllers\ActivitiesController::class);

//peticiones independientes
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/img_payment/{img}', [App\Http\Controllers\PaymentsController::class, 'imag'])->name('img_payment');

