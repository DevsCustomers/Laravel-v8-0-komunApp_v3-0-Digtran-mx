<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Concept;

class ConceptSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $concept = new Concept();
        $concept->name = "comr";
        $concept->description = "Cano of monthly rent";
        $concept->save();

        $concept = new Concept();
        $concept->name = "vpm";
        $concept->description = "Visitor parking meter";
        $concept->save();

        $concept = new Concept();
        $concept->name = "hzm";
        $concept->description = "Humidity zone maintenance";
        $concept->save();

        $concept = new Concept();
        $concept->name = "ma";
        $concept->description = "Maintenance Accensors";
        $concept->save();

        $concept = new Concept();
        $concept->name = "pag";
        $concept->description = "Pruner and Gardening";
        $concept->save();

        $concept = new Concept();
        $concept->name = "ac";
        $concept->description = "Administration cats";
        $concept->save();

        $concept = new Concept();
        $concept->name = "se";
        $concept->description = "Special events";
        $concept->save();       
    }
}
