<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\models\User;
use App\models\Role;
use App\models\Detail;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Administrador';
		$user->email = 'admin@admin.com';
		$user->password = Hash::make('@admin2020');
        $detail = new Detail();

        if($detail->save()){            
            $user->detail_id = $detail->id;
        }

		$user->save();

        $role_user = Role::where('name', 'admin')->first();
        $user->roles()->attach($role_user);
    }
}
