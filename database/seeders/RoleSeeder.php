<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = "admin";
        $role->description = "Administrator";
        $role->save();

        $role = new Role();
        $role->name = "direct";
        $role->description = "Board of Directors";
        $role->save();

        $role = new Role();
        $role->name = "resident";
        $role->description = "Resident";
        $role->save();
    }
}
