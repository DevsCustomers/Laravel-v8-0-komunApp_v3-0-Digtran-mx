<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Parkingmeter;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ParkingmetersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin','direct', 'resident']);

        if($userAuth->hasRole('admin')){
            $parkingmeters = Parkingmeter::all();
        }elseif($userAuth->hasRole('direct')){
            $parkingmeters = Parkingmeter::all();
            $parkingData = [];
            foreach($parkingmeters as $parkingmeter){
                if($parkingmeter->user->detail->conjunto == $userAuth->detail->conjunto){
                    array_push($parkingData, $parkingmeter);
                }                
            }
            $parkingmeters = $parkingData;
        }elseif($userAuth->hasRole('resident')){
            $parkingmeters = $userAuth->parkingmeters;
        }       

        return view('parking_meter.index', ['parkingmeters'=>$parkingmeters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->authorizeRoles(['admin','direct', 'resident']);

        $parkingmeter = new Parkingmeter();
        
        return view('parking_meter.create', ['parkingmeter'=>$parkingmeter]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin','direct', 'resident']);

        $code = Parkingmeter::generarCodigo(12);

        $request->request->add(['code' => $code]);
        
        $request->validate([
            'name' => 'required|string',
            'date' => 'required|date',
            'code' => 'required|unique:parkingmeters,code'           
        ]);

        $parkingmeter = [];

        $parkingmeter['name'] = $request->name;
        $parkingmeter['date'] = $request->date;
        $parkingmeter['code'] = $request->code;

        if($userAuth->hasAnyRole(['admin', 'direct'])){
            if($request->status === 'INVALID' || $request->status === 'VALID' || $request->status === 'COMPLETED'){
                $parkingmeter['status'] = $request->status;
            }
        }else{
            $parkingmeter['status'] = 'INVALID';
        }
              
        $parkingmeter['user_id'] = Auth::id();

        $parking = Parkingmeter::create($parkingmeter);

        if($parking){
            return redirect('/parkingmeters');
        }

        alert()->html('Error','the parking meter could not be created, reload the page and try again','error')->showConfirmButton();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->authorizeRoles(['admin','direct', 'resident']);
        
        $parkingmeter = Parkingmeter::find($id);

        return view('parking_meter.show', ['parkingmeter'=>$parkingmeter]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->authorizeRoles(['admin','direct', 'resident']);

        $parkingmeter = Parkingmeter::find($id);

        return view('parking_meter.edit', ['parkingmeter'=>$parkingmeter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin','direct', 'resident']);

        $request->validate([
            'name' => 'required|string',
            'date' => 'required|date',           
        ]);
        
        $parkingmeter = Parkingmeter::find($id);

        $parkingmeter->name = $request->name;
        $parkingmeter->date = $request->date;
        if($userAuth->hasAnyRole(['admin', 'direct'])){
            if($request->status === 'INVALID' || $request->status === 'VALID' || $request->status === 'COMPLETED'){
                $parkingmeter->status = $request->status;
            }
        }else{
            if($parkingmeter->user_id == Auth::id()){
                $parkingmeter->user_id = Auth::id();
            }else{
                return back();
            }
        }      
       
        if($parkingmeter->save()){
            return redirect('/parkingmeters');
        }

        alert()->html('Error','the parking meter could not be updated, reload the page and try again','error')->showConfirmButton();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userAuth->authorizeRoles(['admin', 'resident']);
        $parkingmeter = Parkingmeter::find($id);

        if($parkingmeter){
            if($parkingmeter->delete()){
                return redirect('/parkingmeters');
            }
        }
        
        alert()->html('Error','the parking meter could not be delete, reload the page and try again','error')->showConfirmButton();
        
        return back();
    }
}
