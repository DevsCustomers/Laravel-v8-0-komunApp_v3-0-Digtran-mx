<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin', 'resident', 'direct']);
        if($userAuth->hasRole('admin')){
            $messages = Message::all();
        }else{
            $messages = Message::where('to_id', $userAuth->id)->get();
        }
        
        return view('message.index', ['messages' => $messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin']);

        $message = new Message();

        $tos = User::where('id', '!=', Auth::id())->get();
        
        if(!$tos->isEmpty()){
            return view('message.create', ['message' => $message, 'tos'=>$tos]);
        }
        
        alert()->html('Error','there are no users to send messages to','error')->showConfirmButton();
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin']);
        $request->validate([
            'data' => 'required',
            'to.*' => 'required',
            'data-text' => 'required|max:500',
        ]);

        $tos = [];

        if(!is_array($request->to)){
            array_push($tos, $request->to);
        }else{
            $tos = $request->to;
        }

        $errorMessageSend = [];

        foreach($tos as $key => $value){
            $message = [];
            $message['message'] = $request->data;
            $message['affair'] = $request->affair; 
            $message['from_id'] = Auth::id();
            $message['to_id'] = $value;

            $message = Message::create($message);

            if(!$message){
                array_push($errorMessageSend, 'Failed to send to:'.$message->to->email);
            }
        }

        if(!empty($errorMessageSend)){
            alert()->html('Error',implode(', ', $errorMessageSend),'error')->showConfirmButton();
        }

        return redirect('/messages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin', 'resident', 'direct']);
        $message = Message::find($id);

        return view('message.show', ['message' => $message]); 
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userAuth = Auth::user();
        $userAuth->authorizeRoles(['admin']);
        $message = Message::find($id);

        if($message){
            if($message->delete()){
                return redirect('/messages');
            }
        }

        alert()->html('Error','the message could not be delete, reload the page and try again','error')->showConfirmButton();
        
        return back();
    }
}
