<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\ReleasesCreated;
use App\Models\Release;
use App\Models\Role;
use App\Models\Detail;
use App\Models\Concept;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ReleasesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = Auth::user();
        $releases = Release::all();
        $user->authorizeRoles(['admin', 'direct', 'resident']);
        if(!$user->hasRole('admin')){
            $releasesUsers = [];
            foreach($releases as $key => $release){
                $conjuntos = explode(',',$release->conjunto);
                if(is_array($conjuntos)){
                    $clave = array_search($user->detail->conjunto, $conjuntos);
                    if($clave !== false){
                        array_push($releasesUsers, $release);
                    }
                }else{
                    if($conjuntos === $user->detail->conjunto){
                        array_push($releasesUsers, $release);
                    }                    
                }  
            }
                                          
            return view('release.index_other', ['releases'=>$releasesUsers]);
        }
        
        return view('release.index', ['releases'=>$releases]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->authorizeRoles(['admin']);
        $release = new Release();

        $conjuntos = Detail::all()->unique('conjunto')->where('conjunto','!=', null);

        if(sizeof($conjuntos) == 0){
            //indica que no hay ningun usuario creado con un conjunto asignado
            alert()->html('Error','there are no users to send release to','error')->showConfirmButton();
            return back();
        }

        $concepts = Concept::all();

        $roles = Role::where('name', '!=', 'admin')->get();       
        
        return view('release.create', ['release'=>$release, 'roles' => $roles, 'conjuntos'=>$conjuntos, 'concepts'=>$concepts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        Auth::user()->authorizeRoles(['admin']);
        $request->validate([
            'data' => 'required',
            'to' => 'required|filled',
            'to.*' => 'required|string|min:2',
            'data-text' => 'required|max:500',
            'conjunto' => 'required|filled',
            'conjunto.*' => 'required|string|min:2',
            'concept' => 'required|filled',
            'concept.*' => 'required|string|min:2',
        ]);

        $arrayConjunto = [];

        if(!is_array($request->conjunto)){
            array_push($arrayConjunto, $request->conjunto);
        }else{
            $arrayConjunto = $request->conjunto;
        }

        $arrayTo = [];
        if(!is_array($request->to)){
            array_push($arrayTo, $request->to);
        }else{
            $arrayTo = $request->to;
        }

        $arrayConcept = [];

        if(!is_array($request->concept)){
            array_push($arrayConcept, $request->concept);
        }else{
            $arrayConcept = $request->concept;
        }

        $release = [];
        $release['data'] = $request->data;
        $release['conjunto'] = implode(", ", $arrayConjunto);
        $release['user_id'] = Auth::id();
        $valRelease = Release::create($release);  
        if(!$valRelease){
            //huvo un error al guardar el comunicado
            alert()->html('Error','the releases could not be created, reload the page and try again','error')->showConfirmButton();
            return back();
        } 

        $errorReleasesSend = [];

        foreach($arrayConcept as $key => $val){
            $concept_release = Concept::where('name', $val)->first();
            if($concept_release){
                $valRelease->concepts()->attach($concept_release);
            }else{
                array_push($errorReleasesSend, 'Failed to send concept to: '.$val); 
            }                    
        }

        foreach($arrayTo as $key => $val2){
            $role_release = Role::where('name', $val2)->first();
            if($role_release){
                $valRelease->roles()->attach($role_release);
            }else{
                array_push($errorReleasesSend, 'Failed to send roles email to: '.$val2); 
            }                   
        }

        if(!empty($errorReleasesSend)){
            alert()->html('Error',implode(', ', $errorReleasesSend),'error')->showConfirmButton();
            return back();
        }
        
        event(new ReleasesCreated($valRelease));
        return redirect('/releases');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);        
        $release = Release::find($id);

        $storedArray = [];
        foreach($release->concepts as $concept){
            if(isset($concept) && $concept->name != ''){
                array_push($storedArray, $concept->description);
            }            
        }
        
        $concepts = implode(', ', $storedArray);
        return view('release.show',['release'=>$release, 'concepts'=>$concepts]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->authorizeRoles(['admin']);
        $release = Release::find($id);

        $conjuntos = Detail::all()->unique('conjunto')->where('conjunto','!=', null);

        if(sizeof($conjuntos) == 0){
            //indica que no hay ningun usuario creado con un conjunto asignado
            alert()->html('Error','there are no users to send release to','error')->showConfirmButton();
            return back();
        }

        $concepts = Concept::all();

        $roles = Role::where('name', '!=', 'admin')->get();      

        return view('release.edit',['release'=>$release, 'roles' => $roles, 'conjuntos'=>$conjuntos, 'concepts'=>$concepts]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $request->validate([
            'data' => 'required',
            'to' => 'required|filled',
            'to.*' => 'required|string|min:2',
            'data-text' => 'required|max:500',
            'conjunto' => 'required|filled',
            'conjunto.*' => 'required|string|min:2',
            'concept' => 'required|filled',
            'concept.*' => 'required|string|min:2',
        ]);       

        $arrayConjunto = [];

        if(!is_array($request->conjunto)){
            array_push($arrayConjunto, $request->conjunto);
        }else{
            $arrayConjunto = $request->conjunto;
        }

        $arrayTo = [];
        if(!is_array($request->to)){
            array_push($arrayTo, $request->to);
        }else{
            $arrayTo = $request->to;
        }

        $arrayConcept = [];

        if(!is_array($request->concept)){
            array_push($arrayConcept, $request->concept);
        }else{
            $arrayConcept = $request->concept;
        }

        $release = Release::find($id);
        $release->data = $request->data;
        $release->conjunto = implode(",", $arrayConjunto);
        $release->user_id = Auth::id();

        $release->concepts()->detach();
        $release->roles()->detach();
        
        $errorReleasesSend = [];

        foreach($arrayConcept as $key => $val){
            $concept_release = Concept::where('name', $val)->first();
            if($concept_release){
                $release->concepts()->attach($concept_release);
            }else{
                array_push($errorReleasesSend, 'Failed to send concept to: '.$val);
            }                     
        }

        foreach($arrayTo as $key => $val2){
            $role_release = Role::where('name', $val2)->first();
            if($role_release){
                $release->roles()->attach($role_release);
            }else{
                array_push($errorReleasesSend, 'Failed to send roles to: '.$val2);   
            }                  
        }

        if(!empty($errorReleasesSend)){
            alert()->html('Error',implode(', ', $errorReleasesSend),'error')->showConfirmButton();
            return back();
        }

        if(!$release->save()){
            //huvo un error al guardar el comunicado
            alert()->html('Error','the releases could not be updated, reload the page and try again','error')->showConfirmButton();
            return back();
        } 
        
        return redirect('/releases');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->authorizeRoles(['admin']);
        $release = Release::find($id);

        if($release){
            if($release->delete()){
                $release->roles()->delete();
                $release->concepts()->delete();
                return redirect('/releases');
            }
        } 
        
        alert()->html('Error','the releases could not be delete, reload the page and try again','error')->showConfirmButton();
        
        return back();
    }
}
