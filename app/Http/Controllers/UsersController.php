<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Detail;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin']);
        $users = User::all();
        return view('user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->authorizeRoles(['admin']);
        $user = new User();

        $roles = Role::all();

        return view('user.create', ['user'=>$user, 'roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        Auth::user()->authorizeRoles(['admin']);
        $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'rol' => ['required'],            
            'name' => ['nullable', 'string'],
            'lastname' => ['nullable', 'string'],
            'conjunto' => ['required', 'string'],
            'phone' => ['nullable', 'regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'],
            'address' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'postal' => ['nullable', 'regex:/^(?:0[1-9]|[1-4]\d|5[0-2])\d{3}$/'],
            'about' => ['nullable', 'string', 'max:500'],
        ]);

        $details = new Detail();

        if(!empty($request->name)){
            $details->name = $request->name;
        }
        if(!empty($request->lastname)){
            $details->lastname = $request->lastname;
        }
        if(!empty($request->conjunto)){
            $details->conjunto = $request->conjunto;
        }
        if(!empty($request->phone)){
            $details->phone = $request->phone;
        }
        if(!empty($request->address)){
            $details->address = $request->address;
        }
        if(!empty($request->city)){
            $details->city = $request->city;
        }
        if(!empty($request->country)){
            $details->country = $request->country;
        }
        if(!empty($request->postal)){
            $details->postal = $request->postal;
        }
        if(!empty($request->about)){
            $details->about = $request->about;
        }

        $details->user_id = Auth::id();

        if($details->save()){
            $user = User::create([
                'name' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'detail_id' => $details->id,
            ]);
    
            $role_user = Role::where('name', $request->rol)->first();
            $user->roles()->attach($role_user);

            return redirect('/users');
        }       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $user = User::find($id);

        return view('user.show',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);       
        $user = User::find($id);

        $roles = Role::all();

        if($user){
            return view('user.edit', ['user'=>$user, 'roles'=>$roles]);
        }

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userEdit = Auth::user();
        $userEdit->authorizeRoles(['admin', 'direct', 'resident']);      

        $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'rol' => ['nullable'],            
            'name' => ['nullable', 'string'],
            'lastname' => ['nullable', 'string'],
            'conjunto' => ['nullable', 'string'],
            'phone' => ['nullable', 'regex:/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'],
            'address' => ['nullable', 'string'],
            'city' => ['nullable', 'string'],
            'country' => ['nullable', 'string'],
            'postal' => ['nullable', 'regex:/^(?:0[1-9]|[1-4]\d|5[0-2])\d{3}$/'],
            'about' => ['nullable', 'string', 'max:500'],
        ]);

        $user = User::find($id);  
        
        if(!empty($request->username)){
            $user->name = $request->username;
        }
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }
        if(!empty($request->name)){
            $user->detail->name = $request->name;
        }
        if(!empty($request->lastname)){
            $user->detail->lastname = $request->lastname;
        }
        if(!empty($request->conjunto) && $userEdit->hasRole('admin')){
            $user->detail->conjunto = $request->conjunto;
        }
        if(!empty($request->phone)){
            $user->detail->phone = $request->phone;
        }
        if(!empty($request->address)){
            $user->detail->address = $request->address;
        }
        if(!empty($request->city)){
            $user->detail->city = $request->city;
        }
        if(!empty($request->country)){
            $user->detail->country = $request->country;
        }
        if(!empty($request->postal)){
            $user->detail->postal = $request->postal;
        }
        if(!empty($request->about)){
            $user->detail->about = $request->about;
        }

        $user->detail->user_id = Auth::id();      
        
        if($user->detail->save()){            
            if($user->save()){
                if($userEdit->hasRole('admin')){
                    $admin = Role::where('name', 'admin')->first();                    
                    if($admin){
                        $firtsAdmin = $admin->users;                       
                        if(count($firtsAdmin) > 1 || !$user->hasRole('admin')){
                            $user->roles()->detach();
                            $role_user = Role::where('name', $request->rol)->first();
                            $user->roles()->attach($role_user);
                            return redirect('/users');
                        }                        
                    }
                    alert()->html('Error','this user is the last admin, his role cannot be edited','error')->showConfirmButton();
                    return back();
                }
                return redirect('/profile');                         
            }
        }      
        alert()->html('Error','the user could not be updated, reload the page and try again','error')->showConfirmButton();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->authorizeRoles(['admin']);
        $user = User::find($id);
        
        if($user){
            $detail = $user->detail->delete();
            $user->delete();
            $user->payments->delete();
        }       
        
        return redirect('/users');
    }
}
