<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activitie;
use App\Models\Schedule;
use Illuminate\Support\Facades\Auth;

class ActivitiesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = new Activitie();

        $activity->title = $request->title;      
		$activity->color = $request->color;
        $activity->date_start = $request->start; 
        $activity->date_end = $request->end;
        $activity->user_id = Auth::id();   

        $schedule = Schedule::find($request->schedule);       

        if($schedule){
            $temp = $activity->save();                     
            if($temp){
                $schedule->activities()->attach($activity);                
                return $activity;
            }
            
        }        
        
        return  'no entra';
    }   

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity = Activitie::find($id);

        $activity->title = $request->title;      
		$activity->color = $request->color;
        $activity->description = $request->description;
        $activity->date_start = $request->start; 
        $activity->date_end = $request->end;
        $activity->user_id = Auth::id();
        
        $schedule = Schedule::find($request->schedule);       

        if($schedule){
            $temp = $activity->save();                     
            if($temp){
                $schedule->activities()->detach($activity->id);
                $schedule->activities()->attach($activity);                
                return $activity;
            }            
        }
        
        return  'no entra';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activitie = Activitie::find($id);

        $activitie->delete();       

        return $activitie;
    }
}
