<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Concept;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Response;

class PaymentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);        

        if(!Auth::user()->hasRole('admin')){
            $payments = Payment::where('user_id', Auth::id())->get();
        }else{
            $payments = Payment::all();
        }     

        return view('payment.index', ['payments'=> $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $payment = new Payment();
        $concepts = Concept::all();
        return view('payment.create', ['payment'=>$payment, 'concepts'=> $concepts]);
    }

    public function imag($filename){
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);

        $path = storage_path('app/images/'.$filename);

        if (!\File::exists($path)) abort(404);
        
        $file = \File::get($path);

        $type = \File::mimeType($path);

        $response = Response::make($file,200);

        $response->header("Content-Type",$type);

        return $response;        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $hasFile = $request->hasFile('view') && $request->view->isValid();  

        $request->validate([
            'comment' => 'required',
            'concept' => 'required',
            'view' => 'required|image',
        ]);
       
        if ($hasFile) {
            # code...            
            $ext = $request->view->extension();  
            
            $archivo = Auth::user()->id.''.Carbon::now()->timestamp;
            $request->view->storeAs('images', "$archivo.$ext");

            $payment = [];
            $payment['user_id'] = Auth::id();
            $payment['coment'] = $request->comment;
            $payment['file'] = $archivo.'.'.$ext;
            $payment['extension'] = $ext;    
            $valPayment = Payment::create($payment);
            if($valPayment){
                $concept_payment = Concept::where('name', $request->concept)->first();
    
                if($concept_payment){
                    $valPayment->concepts()->detach();
                    $valPayment->concepts()->attach($concept_payment);
                }
    
                return redirect('/payments');
                
            }              
        }
        alert()->html('Error','the payments could not be created, reload the page and try again','error')->showConfirmButton();
        return back(); 
    }    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $payment = Payment::find($id);
        return view('payment.show', ['payment'=>$payment]);
    }  
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->authorizeRoles(['admin']);
        $payment = Payment::find($id);

        if($payment){
            if($payment->delete()){
                $payment->concepts()->delete();
                return redirect('/payments');
            }
        }     
        
        alert()->html('Error','the payments could not be delete, reload the page and try again','error')->showConfirmButton();
        return back();        
    }
}
