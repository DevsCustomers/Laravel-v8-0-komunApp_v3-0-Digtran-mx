<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\User;
use RealRashid\SweetAlert\Facades\Alert;

class ProfilesController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $user = Auth::user();
        return view('profile.index', ['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Auth::user()->authorizeRoles(['admin', 'direct', 'resident']);
        $user = User::find($id);

        $roles = Role::all();

        if($user){
            return view('profile.edit', ['user'=>$user, 'roles'=>$roles]);
        }

        return back();
    }
}
