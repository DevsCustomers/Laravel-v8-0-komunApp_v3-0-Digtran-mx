<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\Detail;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class SchedulesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $schedules = Schedule::all();

        if($user->hasAnyRole(['admin'])){
            return view('schedule.index', ['schedules'=>$schedules]);
        }

        $conjunto = $user->detail->conjunto;        

        $calendar = '';

        foreach($schedules as $schedule){
            $conjuntos = explode(',', $schedule->conjunto);
            if(is_array($conjuntos)){               
                if(in_array($conjunto, $conjuntos)){
                    $calendar = $schedule;
                    break;
                }
            }else{
                $conjuntotemp = $conjuntos;
                if($conjunto == $conjuntotemp){
                    $calendar = $schedule;
                    break;
                }
            }           
        }
        $schedule = $calendar;
        
        if($calendar !== ''){
            $calendarData = $schedule->fullcalendarData();
            return view('schedule.show', ['schedule'=>$schedule, 'dataCalendar' => $calendarData]); 
        }       
        
        alert()->html('Error','there are no users to created schedules to','error')->showConfirmButton();
        return back();        
              
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $schedule = new Schedule();

        $conjuntos = Detail::all()->unique('conjunto')->where('conjunto','!=', null);

        if(sizeof($conjuntos) == 0){
            //indica que no hay ningun usuario creado con un conjunto asignado
            alert()->html('Error','there are no users to created schedules to','error')->showConfirmButton();
            return back();
        }

        return view('schedule.create', ['schedule'=>$schedule, 'conjuntos'=>$conjuntos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'conjunto.*' => 'required|string',
        ]);

        $schedule = new Schedule();

        $schedule->title = $request->title;
        if(is_array($request->conjunto)){
            $schedule->conjunto = implode(', ', $request->conjunto);
        }else{
            $schedule->conjunto = $request->conjunto;
        }

        $schedule->user_id = Auth::id();

        if($schedule->save()){
           return redirect('schedules');
        }
        
        alert()->html('Error','the schedule could not be created, reload the page and try again','error')->showConfirmButton();

        return back();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $schedule = Schedule::find($id);
        $calendarData = $schedule->fullcalendarData();      
        return view('schedule.show', ['schedule'=>$schedule, 'dataCalendar' => $calendarData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $schedule = Schedule::find($id);
        $conjuntos = Detail::all()->unique('conjunto')->where('conjunto','!=', null);

        if(sizeof($conjuntos) == 0){
            //indica que no hay ningun usuario creado con un conjunto asignado
            alert()->html('Error','there are no users to updated schedules to','error')->showConfirmButton();
            return back();
        }
        return view('schedule.edit', ['schedule'=>$schedule, 'conjuntos'=>$conjuntos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string',
            'conjunto.*' => 'required|string',
        ]);

        $schedule = Schedule::find($id);

        if($schedule){
            $schedule->title = $request->title;

            if(is_array($request->conjunto)){
                $schedule->conjunto = implode(',', $request->conjunto);
            }else{
                $schedule->conjunto = $request->conjunto;
            }

            $schedule->user_id = Auth::id();

            if($schedule->save()){
            return redirect('/schedules');
            }
        } 
        
        alert()->html('Error','the schedule could not be updated, reload the page and try again','error')->showConfirmButton();

        return back(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Auth::user()->authorizeRoles(['admin']);
        $schedule = Schedule::find($id);

        if($schedule){
            if($schedule->delete()){                
                return redirect('/schedules');
            }
        } 
        
        alert()->html('Error','the schedule could not be delete, reload the page and try again','error')->showConfirmButton();
        
        return back();
    }
}
