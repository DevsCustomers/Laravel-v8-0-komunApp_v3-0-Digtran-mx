<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\ReleasesCreated;
use App\Listeners\ReleasesSendNotify;
use App\Models\Release;
use App\Models\Payment;
use App\Models\Parkingmeter;
use App\Notifications\ReleaseNotification;
use App\Models\Detail;
use App\Models\Message;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ParkingmeterNotification;
use App\Notifications\PaymentNotification;
use App\Notifications\MessageNotification;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ReleasesCreated::class => [
            ReleasesSendNotify::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {        
        Release::updated(function($release){
            $data = [];
            $data['subject'] = 'updated Release';            
            $data['line1'] = 'a Release statement has been updated, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';
            $conjuntos = explode(', ', $release->conjunto);

            $roles_release = $release->roles;

            $roles = [];                            
            foreach($roles_release as $rol){
                array_push($roles, $rol->name);
            }
            $roles = implode(', ', $roles);

            foreach($conjuntos as $conjunto){
                $temp = Detail::where('conjunto', trim($conjunto))->get();
                if($temp){                    
                    foreach($temp as $tp){                     
                        if($tp->user->hasAnyRole($roles) && Auth::id() != $tp->user->id){
                            $data['greeting'] = 'Hello '.$tp->name;                            
                            $tp->user->notify(new ReleaseNotification($release, $data));
                        }                            
                    }                                     
                }                
            }            
        });

        Payment::created(function($payment){
            $data = [];
            $data['subject'] = 'new Payment';            
            $data['line1'] = 'a Payment statement has been created, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';

            $roleAdm = Role::where('name', 'admin')->first();
            $usersAdm = $roleAdm->users;
            $usersData = [];
            if(!is_array($usersAdm)){
                $usersData = $usersAdm;
            }else{
                array_push($usersData, $usersAdm);
            }

            foreach($usersData as $user){
                $data['greeting'] = 'Hello '.$user->detail->name;
                $user->notify(new PaymentNotification($payment, $data));
            }
        });

        Payment::updated(function($payment){
            $data = [];
            $data['subject'] = 'updated Payment';            
            $data['line1'] = 'a Payment statement has been updated, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';

            $user = $payment->user;
            $data['greeting'] = 'Hello '.$user->detail->name;
            $user->notify(new PaymentNotification($payment, $data));
        });

        Parkingmeter::created(function($parkingmeter){
            $data = [];
            $data['subject'] = 'new Parkingmeter';            
            $data['line1'] = 'a Parkingmeter statement has been created, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';

            $roleAdm = Role::where('name', 'admin')->first();
            $usersAdm = $roleAdm->users;
            $usersData = [];
            if(!is_array($usersAdm)){
                $usersData = $usersAdm;               
            }else{
                array_push($usersData, $usersAdm);
            }           

            foreach($usersData as $user){
                $data['greeting'] = 'Hello '.$user->detail->name;
                $user->notify(new ParkingmeterNotification($parkingmeter, $data));
            }

            $conjuntoUserParking = $parkingmeter->user->detail->conjunto;

            $roleDirect = Role::where('name', 'direct')->first();            
            $usersDirect = $roleDirect->users;
            $usersData = [];
            if(!is_array($usersDirect)){
                $usersData = $usersDirect;
            }else{
                array_push($usersData, $usersDirect);
            }

            foreach($usersData as $user){
                if($user->detail->conjunto === $conjuntoUserParking){
                    $data['greeting'] = 'Hello '.$user->detail->name;
                    $user->notify(new ParkingmeterNotification($parkingmeter, $data));
                }                
            }
        });

        Parkingmeter::updated(function($parkingmeter){
            $data = [];
            $data['subject'] = 'updated Parkingmeter';            
            $data['line1'] = 'a Parkingmeter statement has been updated, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';

            $user = $parkingmeter->user;
            $data['greeting'] = 'Hello '.$user->detail->name;
            $user->notify(new ParkingmeterNotification($parkingmeter, $data));
        });

        Message::created(function($message){
            $from = $message->userFrom;
            $to = $message->userTo;           
            $data = [];
            $data['subject'] = 'new Message';            
            $data['line1'] = 'a Message statement has been created, you should check it out';
            $data['line2'] = 'any questions or suggestions contact an administrator';
            $data['greeting'] = 'Hello '.$to->detail->name;
            $to->notify(new MessageNotification($message, $data));
        });
    }
}
