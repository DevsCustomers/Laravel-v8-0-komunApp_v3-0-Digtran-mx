<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Detail;
use App\Notifications\ReleaseNotification;
use Illuminate\Support\Facades\Auth;
class ReleasesSendNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $release = $event->release;
        $data = [];
        $data['subject'] = 'New Release';            
        $data['line1'] = 'a new statement has been created, you should check it out';
        $data['line2'] = 'any questions or suggestions contact an administrator';
        $conjuntos = explode(', ', $release->conjunto);

        $roles_release = $release->roles;

        $roles = [];                            
        foreach($roles_release as $rol){
            array_push($roles, $rol->name);
        }
        $roles = implode(', ', $roles);
        foreach($conjuntos as $conjunto){            
            $temp = Detail::where('conjunto', trim($conjunto))->get();
            if($temp){               
                foreach($temp as $tp){                   
                    if($tp->user->hasAnyRole($roles) && Auth::id() != $tp->user->id){
                        $data['greeting'] = 'Hello '.$tp->name;                               
                        $tp->user->notify(new ReleaseNotification($release, $data));                                                          
                    }                            
                }                          
            }                
        }        
    }
}
