<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'coment',
        'file',
        'extension',
    ];


    public function scopeLatest($query){
        return $query->orderBy("id", "Desc");
    }
    
    /**
     * The concepts that belong to the payment.
     */
    public function concepts()
    {
        return $this->belongsToMany(Concept::class, 'concept_payment', 'payment_id', 'concept_id');
    }

    public function hasConcept($concept){
        if($this->concepts()->where('name', $concept)->first()){
            return true;
        }
        return false;
    }

    /**
     * The releases that belong to the user.
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
