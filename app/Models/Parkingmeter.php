<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parkingmeter extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'date',
        'code',
        'status',
        'user_id',
    ];

    /**
     * The releases that belong to the user.
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    static function generarCodigo($longitud) {
	    $key = '';
	    $pattern = '123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ';
	    $max = strlen($pattern)-1;
	    for($i=0;$i < $longitud;$i++){
	    	$key .= $pattern[mt_rand(0,$max)];
	    }
	    return $key;
	}
}
