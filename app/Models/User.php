<?php

namespace App\Models;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * Get the user associated with the detail.
     */
    public function messageTo()
    {
        return $this->hasOne(Message::class, 'to_id');
    }

    /**
     * Get the user associated with the detail.
     */
    public function messageFrom()
    {
        return $this->hasOne(Message::class, 'from_id');
    }

    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * The roles that belong to the user.
     */
    public function parkingmeters()
    {
        return $this->belongsToMany(Parkingmeter::class);
    }

    /**
     * Get the releases for the blog post.
     */
    public function releases()
    {
        return $this->hasMany('App\Models\Release');
    }

    /**
     * Get the releases for the blog post.
     */
    public function payments()
    {
        return $this->hasMany('App\Models\Release');
    }

    /**
     * Get the details associated with the user.
     */
    public function detail()
    {
        return $this->belongsTo('App\Models\Detail');
    }

    /**
     * authorize the roles associated with the user.
     */
    public function authorizeRoles($roles){
        if($this->hasAnyRole($roles)){
            return true;
        }
        abort(401, 'Esta acción no esta permitida');
    }

    public function hasAnyRole($roles){
        if(is_array($roles)){
            foreach ($roles as $role) {
                if($this->hasRole($role)){
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }

        return false;
    }

    public function hasRole($role){
        if($this->roles()->where('name', $role)->first()){
            return true;
        }
        return false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'detail_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
