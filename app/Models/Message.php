<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
        'affair',
        'from_id',
        'to_id',
    ];

    /**
     * Get the user that owns the phone.
     */
    public function userTo()
    {
        return $this->belongsTo(User::class, 'to_id');
    }

    /**
     * Get the user that owns the phone.
     */
    public function userFrom()
    {
        return $this->belongsTo(User::class, 'from_id');
    }
}
