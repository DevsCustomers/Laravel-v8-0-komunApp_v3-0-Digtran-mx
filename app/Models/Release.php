<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use App\models\Detail;
use Illuminate\Database\Eloquent\SoftDeletes;

class Release extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'conjunto',
        'user_id',
    ];


    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_release', 'release_id', 'role_id');
    }

    /**
     * The roles that belong to the user.
     */
    public function concepts()
    {
        return $this->belongsToMany(Concept::class, 'concept_release', 'release_id', 'concept_id');
    }

    /**
     * The releases that belong to the user.
     */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function hasConcept($concept){
        if($this->concepts()->where('name', $concept)->first()){
            return true;
        }
        return false;
    }

    public function getConceptsImplode(){
        $storedArray = [];
        foreach($this->concepts as $concept){
            if(isset($concept) && $concept->name != ''){
                array_push($storedArray, $concept->description);
            }            
        }
        
        $concepts = implode(', ', $storedArray);
        
        return $concepts;
    }

    public function getRoles(){
        $storedArray = [];
        foreach($this->roles as $rol){
            if(isset($rol) && $rol->name != ''){
                array_push($storedArray, $rol->name);
            }            
        }

        return $storedArray;
    }
    
    public function hasConjunto($conjunto){
        $conjuntoTemp = explode(",", $this->conjunto);
        $bandera = false;
        if(is_array($conjuntoTemp)){
            foreach($conjuntoTemp as $key => $value){
                if($conjunto == $value){
                    $bandera = true;
                }
            }
        }

        return $bandera;
    }

    public function hasAnyRole($roles){
        if(is_array($roles)){
            foreach ($roles as $role) {
                if($this->hasRole($role)){
                    return true;
                }
            }
        }else{
            if($this->hasRole($roles)){
                return true;
            }
        }

        return false;
    }

    public function hasRole($role){
        if($this->roles()->where('name',$role)->first()){
            return true;
        }
        return false;
    }
}
