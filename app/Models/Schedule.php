<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Get the details associated with the user.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function hasConjunto($conjunto){
        $conjuntoTemp = explode(",", $this->conjunto);
        $bandera = false;
        if(is_array($conjuntoTemp)){
            foreach($conjuntoTemp as $key => $value){
                if($conjunto == $value){
                    $bandera = true;
                }
            }
        }

        return $bandera;
    }   

    /**
     * The roles that belong to the user.
     */
    public function activities()
    {
        return $this->belongsToMany(Activitie::class);
    }

    /**
     * the array for javascript fullcalendar
     */
    public function fullcalendarData(){
        $activities = $this->activities;
        if(is_iterable($activities)){
            $data = [];
            foreach($activities as $activity){
                $tempActivitie = [];
                $tempActivitie['id'] = $activity->id;                
                $tempActivitie['title'] = $activity->title;
                $tempActivitie['description'] = $activity->description;               
                $tempActivitie['start'] = $activity->date_start;
                $tempActivitie['end'] = $activity->date_end;
                $tempActivitie['allDay'] = true;
                $tempActivitie['className'] = $activity->color;                
                array_push($data, $tempActivitie);
            }

            return $data;
        }else{
            $activities = $activities->first();
            $tempActivitie = [];
            $tempActivitie['id'] = $activities->id;                
            $tempActivitie['title'] = $activities->title;
            $tempActivitie['description'] = $activity->description;                              
            $tempActivitie['start'] = $activities->date_start;
            $tempActivitie['end'] = $activities->date_end;
            $tempActivitie['allDay'] = true;
            $tempActivitie['className'] = $activities->color;
            
            return $tempActivitie;
        }
        
        return false;
    }
}
