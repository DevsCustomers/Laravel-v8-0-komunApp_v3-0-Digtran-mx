<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Payment;
use Illuminate\Database\Eloquent\SoftDeletes;

class Concept extends Model
{
    use HasFactory, SoftDeletes;

    public function payments()
    {
        return $this->belongsToMany(Payment::class);
    }

    public function releases()
    {
        return $this->belongsToMany(Release::class);
    }
}
