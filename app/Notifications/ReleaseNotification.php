<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReleaseNotification extends Notification
{
    use Queueable;

    private $release;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($release, $data)
    {
        $this->release = $release;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->data['subject'])
                    ->greeting($this->data['greeting'])
                    ->line($this->data['line1'])
                    ->action('Notification Action', url('/releases/'.$this->release->id))
                    ->line($this->data['line1']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
