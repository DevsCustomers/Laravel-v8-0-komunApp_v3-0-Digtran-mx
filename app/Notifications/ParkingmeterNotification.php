<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ParkingmeterNotification extends Notification
{
    use Queueable;

    private $parkingmeter;
    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($parkingmeter, $data)
    {
        $this->parkingmeter = $parkingmeter;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->data['subject'])
                    ->greeting($this->data['greeting'])
                    ->line($this->data['line1'])
                    ->action('Notification Action', url('/parkingmeters/'.$this->parkingmeter->id))
                    ->line($this->data['line2']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
